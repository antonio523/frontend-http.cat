<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pagina Principale</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style><%@include file="/META-INF/resources/css/index.css"%> </style>
</head>
<body>
<div class="container">
<div class="row">
 <div class="col-4"></div>
 <div class="col-8">
 <h1>Scegli cosa vuoi fare!</h1> 
    </div>
    
    <div class="col-4"></div>
 <div class="col-8">
<a class="btn btn-success" href="giornaliero" role="button">Cocktail giornaliero!</a> <br> <br> 
    </div>
    
    <div class="col-4"></div>
 <div class="col-8">
<a class="btn btn-danger" href="random" role="button">Genera un cocktail randomicamente</a> <br> <br>
    </div>
    
      <div class="col-4"></div>
 <div class="col-8">
<a class="btn btn-warning" href="cocktailname" role="button">Cerca un cocktail per nome!</a> <br> <br>
    </div>
    
     <div class="col-4"></div>
 <div class="col-8">
<a class="btn btn-info" href="ingredientname" role="button">Cerca un ingrediente per nome!</a> <br> <br>
    </div>
</div>
 </div>

		
</body>
</html>