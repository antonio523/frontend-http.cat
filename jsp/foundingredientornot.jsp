<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="model.IngredienteDB, java.util.List" %>
<html>
<head>
<meta charset="ISO-8859-1">

<% List<IngredienteDB> ingredienti = (List<IngredienteDB>) request.getAttribute("ingredienti"); %>

<title><%= ingredienti.size()>0?"Ingredienti trovati!":"Nessun Risultato!"%></title>
</head>
<body>
	<% for(IngredienteDB c : ingredienti){ %>
	<h1><%= c.getNome() %></h1>
	<img src="https://s1.best-wallpaper.net/wallpaper/m/1708/Summer-cocktail-fruit-sea-sunset_m.webp" class="img-fluid" alt="Responsive image">
	<% }%>
	
</body>
</html>