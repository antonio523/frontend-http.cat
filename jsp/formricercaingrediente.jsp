<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="model.CocktailDB" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Digita il nome (o parte) dell'ingrediente da cercare!</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style><%@include file="/META-INF/resources/css/formricercaingrediente.css"%> </style>

</head>
<body>

<br>

<div class="container">
<div class="row"> 
<div class="col-4"></div>
 <div class="col-8">
 <h1>Cerca il tuo ingrediente preferito!</h1> 
    </div>
    <div class="col-4"></div>
 <div class="col-5">
<form action="ingredientname" method="POST">
  <input type="text"  id="name" name="name"class="form-control" placeholder="Nome ingrediente"> <br>
  <button type="submit" class="btn btn-dark" value="Cerca l'ingrediente!">Cerca l'ingrediente </button>
  </form>
    </div>
	
  </div>
  </div>
</form>
</body>
</html>