<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="model.CocktailDB, java.util.List" %>
<html>
<head>
<meta charset="ISO-8859-1">

<% List<CocktailDB> cocktails = (List<CocktailDB>) request.getAttribute("cocktail"); %>

<title><%= cocktails.size()>0?"Cocktails trovati!":"Nessun Risultato!"%></title>
</head>
<body>
	<% for(CocktailDB c : cocktails){ %>
	<h1><%= c.getNome() %></h1>
	<img src="https://s1.best-wallpaper.net/wallpaper/m/1708/Summer-cocktail-fruit-sea-sunset_m.webp" class="img-fluid" alt="Responsive image">
	<% }%>
	
</body>
</html>